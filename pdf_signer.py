from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS
import json
import requests
import base64

app = Flask(__name__)
CORS(app)
api = Api(app)

# Token de autenticação gerado no BRy Cloud
AUTHORIZATION = ''

# Endereço de acesso ao HUB-Signer
# URI_HUB = 'https://hub2.hom.bry.com.br'   #Ambiente de homologação
URI_HUB = 'https://hub2.bry.com.br'         #Ambiente de Produção


class assina_KMS(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form

        # Lê o documento enviado para assinatura no front-end
        pdf_assinatura = request.files['documento'].read()

        # Recupera o tipo da credencial enviado
        kms_credencial_tipo = data["tipo_credencial"]

        # Recupera o valor da credencial
        kms_credencial = data["valor_credencial"]

        # Recupera informacao de CPF se enviado na requisição
        signatario_info = ""
        if 'signatario' in data:
            signatario_info = ', "signatario" : "' + data['signatario'] + '"'


        # Verifica se a assinatura possuí imagem e realiza a requisição de acordo com essa condição
        if data['incluirIMG'] == "true":
            image = request.files['imagem'].read()
            signer_form = { # Valor do campo signatario deve ser colocado entre " " na requisicao
            'dados_assinatura': '{"algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '"' + signatario_info + '}',
            'configuracao_imagem': '{"altura" : "' + data['altura'] + '",  "largura" : ' + data['largura'] + ',  "coordenadaX" : ' + data['coordenadaX'] + ',  "coordenadaY" : ' + data['coordenadaY'] + ',  "posicao" : "' + data['posicao'] + '", "pagina" : "' + data['pagina'] + '"}',
            'configuracao_texto': '{"texto" : "' + data['texto'] + '" ,"incluirCN" : ' + data['incluirCN'] + ', "incluirCPF" : ' + data['incluirCPF'] + ', "incluirEmail" : ' + data['incluirEmail'] + '}',
            }
            files = [
                ('documento', pdf_assinatura),
                ('imagem', image)
            ]
        else:
            signer_form = {
                'dados_assinatura': '{ "algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '"' + signatario_info + ' }',
            }
            files = [
                ('documento', pdf_assinatura),
            ]  
        header = {
        'Authorization': AUTHORIZATION,
        'kms_credencial': kms_credencial,
        'kms_credencial_tipo': kms_credencial_tipo
        }

        # Se o tipoProfissional for Médico, precisa inserir os OIDs do tipo farmacêutico junto aos do médico (porém com valores nulos)
        if data['numeroOID'] == "2.16.76.1.4.2.2.1":
            signer_form['metadados'] = '{"'+data['tipoDocumento']+'" : "", "' +data['numeroOID']+'" : "'+data['numero']+'", "'+data['UFOID'] +'" : "'+data['UF']+'", "' +data['especialidadeOID']+'" : "'+data['especialidade']+'", "2.16.76.1.4.2.3.1" : "", "2.16.76.1.4.2.3.2" : "", "2.16.76.1.4.2.3.3" : ""}'
        else:
            signer_form['metadados'] = '{"'+data['tipoDocumento']+'" : "", "' +data['numeroOID']+'" : "'+data['numero']+'", "'+data['UFOID'] +'" : "'+data['UF']+'", "' +data['especialidadeOID']+'" : "'+data['especialidade']+'"}'
        
        print('============= Iniciando assinatura no BRy HUB utilizando certificado em nuvem ... =============')

        response = requests.post(URI_HUB + '/fw/v1/pdf/kms/lote/assinaturas',
                                 data=signer_form, files=files, headers=header)
        if response.status_code == 200:
            print('Assinatura realizada com sucesso!')
            print(response.json())

            return response.json()['documentos'][0]['links'][0]['href']
        else:
            # Retorna mensagem e status do erro do HUB
            print(response.text)
            return response.text, response.status_code

api.add_resource(assina_KMS, '/assinador/assinarKMS')

if __name__ == '__main__':
    app.run(host="localhost", port=8000, debug=True)
